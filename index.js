const { graphqlHandler } = require('graphql-serverless')
const { app } = require('webfunc')
const { makeExecutableSchema } = require('graphql-tools') 
const glue = require('schemaglue')

const { schema, resolver } = glue('src/graphql')

const executableSchema = makeExecutableSchema({
    typeDefs: schema,
    resolvers: resolver
})

const graphqlOptions = {
  schema: executableSchema,
  graphiql: { 
    endpoint: '/graphiql' 
  }
}

app.all(['/', '/graphiql'], graphqlHandler(graphqlOptions))


eval(app.listen('app', 4000))